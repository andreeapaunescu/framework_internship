/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Disp7Seg.c
 *    \author     Andreea-Georgiana Paunescu
 *    \brief
 *
 */
/*-------------------------------------------------------------------------------------------------------------------*/
#define NUMBEROFREADINGS 2

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/
#include "Disp7Seg.h"
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
static Disp7Seg_SegType disp7seg_array[NUMBEROFREADINGS];
static Disp7Seg_DotsType Disp7Seg_Dots;
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
uint8 disp_right;
uint8 disp_left;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/
static void SetDigitalLeft(uint8 left_digit)
{
   if (left_digit == 0)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0X80C0);
   }
   else if (left_digit == 1)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0X80F9);
   }
   else if (left_digit == 2)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0X80A4);
   }
   else if (left_digit == 3)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0X80B0);
   }
   else if (left_digit == 4)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0X8099);
   }
   else if (left_digit == 5)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0X8092);
   }
   else if (left_digit == 6)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0X8082);
   }
   else if (left_digit == 7)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0X80F8);
   }
   else if (left_digit == 8)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0X8080);
   }
   else if (left_digit == 9)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0X8090);
   }
   else if (left_digit == 255)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DIG1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_C, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_G, STD_HIGH);
   }
//   else{
//      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0X8000);
//   }
}

static void SetDigitalRight(uint8 right_digit)
{
   if (right_digit == 0)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0X80C0);
   }
   else if (right_digit == 1)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0X80F9);
   }
   else if (right_digit == 2)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0X80A4);
   }
   else if (right_digit == 3)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0X80B0);
   }
   else if (right_digit == 4)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0X8099);
   }
   else if (right_digit == 5)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0X8092);
   }
   else if (right_digit == 6)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0X8082);
   }
   else if (right_digit == 7)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0X80F8);
   }
   else if (right_digit == 8)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0X8080);
   }
   else if (right_digit == 9)
   {
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0X8090);
   }
   else if (right_digit == 255)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DIG2, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_A, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_B, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_C, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_D, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_E, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_F, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_G, STD_HIGH);
   }
//   else{
//
//            IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, STD_HIGH);
//   }
}

void Disp_Dots()
{
   if (Disp7Seg_Dots == DISP7SEG_BOTH_OFF)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_HIGH);
   }
   else if (Disp7Seg_Dots == DISP7SEG_LEFT_ON)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_HIGH);
   }
   else if (Disp7Seg_Dots == DISP7SEG_RIGHT_ON)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_HIGH);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_LOW);
   }
   else if (Disp7Seg_Dots == DISP7SEG_BOTH_ON)
   {
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_LEFT_DP1, STD_LOW);
      IoHwAb_DigitalSetChannel(IOHWAB_DIGITAL_CHANNEL_DISP7SEG_RIGHT_DP2, STD_LOW);
   }
}
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
void Disp7Seg_Init(void)
{
   for (uint8 i = 0; i < NUMBEROFREADINGS; i++)
   {
      disp7seg_array[i].Disp7Seg_Dots = DISP7SEG_BOTH_OFF;
      disp7seg_array[i].u_DecValue = 0xFF;
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_LEFT_DIGIT, 0x8000);
      IoHwAb_DigitalSetPort(IOHWAB_DIGITAL_PORT_DISP7SEG_RIGHT_DIGIT, 0x8000);
   }
}
void Disp7Seg_MainFunction(void)
{
   SetDigitalRight(disp_right);
   SetDigitalLeft(disp_left);
   Disp_Dots();
}
void Disp7Seg_SetState(uint8 uc_Id, uint8 uc_DecValue, Disp7Seg_DotsType t_Dots)
{
   uint8 value = uc_DecValue;
   if (disp7seg_array[uc_Id].id)
   {
      if (value < 100)
      {
         disp_right = value % 10;
         disp_left = value / 10;
         Disp7Seg_Dots = t_Dots;
      }
      else if (value == 255)
      {
         disp_right = value;
         disp_left = value;
         Disp7Seg_Dots = t_Dots;
      }

   }

}

