//*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Btn_Core.c
 *    \author     Paunescu Andreea-Georgiana
 *    \brief      Implements the behavior of the software component through generic runnables. The behavior can support
 *                multiple instances and the interaction with the RTE is performed through set or get functions.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Btn.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the output values after processing the input values that can be read through the get function. */

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
//declararea unui array de tip ButtonStruct cu 3
static ButtonStruct btn_array[3];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Initializes all the core global variables.
 * \param      -
 * \return     -
 */

void Btn_Init(void)
{
   //initializare butoane

   btn_array[0].State = BTN_NOT_PRESSED;
   btn_array[0].button_index = IOHWAB_DIGITAL_CHANNEL_BTN_LEFT;
   btn_array[0].counter = 0;


   btn_array[1].State = BTN_NOT_PRESSED;
   btn_array[1].button_index = IOHWAB_DIGITAL_CHANNEL_BTN_RIGHT;
   btn_array[1].counter = 0;

   btn_array[2].State = BTN_NOT_PRESSED;
   btn_array[2].button_index = IOHWAB_DIGITAL_CHANNEL_BTN_HAZARD;
   btn_array[2].counter = 0;

}

/**
 * \brief      Implements the cyclic digital input signals processing according to configured active values.
 * \param      -
 * \return     -
 */

void Btn_MainFunction(void)
{
   //aplicarea algoritmului de debounce
   for (uint8 uc_Id = 0; uc_Id < 3; uc_Id++)
   {
      //pentru starea butonui BTN_NOT_PRESSED
      if (btn_array[uc_Id].State == BTN_NOT_PRESSED)
      {
         if (STD_HIGH == IoHwAb_DigitalGetChannel(btn_array[uc_Id].button_index))
         {
            btn_array[uc_Id].counter++;
            if (btn_array[uc_Id].counter > 4)
            {
               btn_array[uc_Id].counter = 0;
               btn_array[uc_Id].State = BTN_PRESSED;

            }
         }
         else
         {
            btn_array[uc_Id].State = BTN_NOT_PRESSED;
            btn_array[uc_Id].counter = 0;
         }
      }
     //pentru starea butonui  BTN_PRESSED
      else
      {
         if (STD_LOW == IoHwAb_DigitalGetChannel(btn_array[uc_Id].button_index))
         {

            btn_array[uc_Id].counter++;
            if (btn_array[uc_Id].counter > 4)
            {
               btn_array[uc_Id].counter = 0;
               btn_array[uc_Id].State = BTN_NOT_PRESSED;
            }
         }
         else
         {
            btn_array[uc_Id].State = BTN_PRESSED;
            btn_array[uc_Id].counter = 0;

         }

      }
   }


}
/**
 * \brief      Returns the current output state of the specified instance.
 * \param      uc_Id : specific ID of a instance.
 * \return     The current output value of the specified instance.
 */
Btn_PressStateType Btn_GetState(uint8 uc_Id)
{
   return btn_array[uc_Id].State;

}
