/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       PotMet.c
 *    \author     Paunescu Andreea Georgiana
 *    \brief      The PotMet (Potentiometer) SWC shall implement a potentiometer driver that
 processes analog input channels and assures filtered and interpolated output states that
 define the position of potentiometers in percentages.
 */
/*-------------------------------------------------------------------------------------------------------------------*/
#define   INPUT_MIN  0
#define   OUTPUT_MIN  0
#define   INPUT_MAX  4095
#define   OUTPUT_MAX  10000
#define   NUMBEROFREADINGS 3
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/
#include "PotMet.h"
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
static PotMet_PositionType buffer[NUMBEROFREADINGS];


/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
static uint16 FINALVALUE;
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
uint16 counter = 0;
static uint16 average()
{

   uint16 avg = 0;
   uint16 uc_Id;

   //pentru cele 3 citiri se realizeaza suma
   for (uc_Id = 0; uc_Id < NUMBEROFREADINGS; uc_Id++)
   {
      avg = avg + buffer[uc_Id];
   }

   //media aritmetica
   avg = avg / NUMBEROFREADINGS;

   return avg;

}

static uint16 interpolare(uint16 INPUT)
{
   uint16 OUTPUT;
   OUTPUT = (uint32) (INPUT - INPUT_MIN) * (OUTPUT_MAX - OUTPUT_MIN) / (INPUT_MAX - INPUT_MIN) + OUTPUT_MIN;
   return OUTPUT;
}

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
void PotMet_Init(void)
{

   buffer[0] = IoHwAb_AnalogGetChannel(IOHWAB_ANALOG_POTENTIOMETER);
   buffer[1] = IoHwAb_AnalogGetChannel(IOHWAB_ANALOG_POTENTIOMETER);
   buffer[2] = IoHwAb_AnalogGetChannel(IOHWAB_ANALOG_POTENTIOMETER);

}
void PotMet_MainFunction(void)
{
   buffer[counter] = IoHwAb_AnalogGetChannel(IOHWAB_ANALOG_POTENTIOMETER);
   uint16 READVALUE = average();
   FINALVALUE = interpolare(READVALUE);

   counter++;
   if (counter > 3)
   {
      counter = 0;
   }
}

PotMet_PositionType PotMet_GetState(uint8 uc_Id)
{
   uc_Id++;
   return FINALVALUE ;
}
