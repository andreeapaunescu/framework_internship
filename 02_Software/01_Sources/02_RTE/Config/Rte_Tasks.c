/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Rte_Tasks.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Implements all the OS tasks. Maps software component runnables to tasks.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Os.h"
#include "IoHwAb.h"
#include "Btn.h"
#include "PotMet.h"
#include "Led.h"
#include "Disp7Seg.h"

/*------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Specifies the upper duty cycle limit to be used in the LEDs dimming. */
#define RTE_DIMMING_DUTY_UPPER_LIMIT           (0x2000UL)
#define CONFIG_LEFT_RIGHT_ON_OFF_PERIODS       (250)
#define Config_Hazard_ON_OFF_Periods           (500)
#define Config_LEDs_Intensity                  (0x8000)
#define CONFIG_LEFT_RIGHT_NR_OF_FLASHES        (4)

#define   INPUT_MIN  0
#define   OUTPUT_MIN  100
#define   INPUT_MAX  10000
#define   OUTPUT_MAX  900
#define   OUTPUT_MIN_CONFIG3  1
#define   OUTPUT_MAX_CONFIG3 100
#define   OUTPUT_MIN_CONFIG4 2
#define   OUTPUT_MAX_CONFIG4 6

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the dimming states of the LEDs. */
typedef enum
{
   RTE_UPWARDS,
   RTE_DOWNWARDS,
} Rte_DimmingType;

typedef enum
{
   RTE_STATE_MACHINE_IDLE,
   RTE_STATE_MACHINE_RIGHT_SIGNALING,
   RTE_STATE_MACHINE_LEFT_SIGNALING,
   RTE_STATE_MACHINE_HAZARD_SIGNALING,
   RTE_STATE_MACHINE_CONFIG1,
   RTE_STATE_MACHINE_CONFIG2,
   RTE_STATE_MACHINE_CONFIG3,
   RTE_STATE_MACHINE_CONFIG4,

} Rte_StateMachineType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

static uint8 counter;
static uint16 counterH;

static uint8 counterForHazard = 0;
static Rte_StateMachineType t_CurrentState = RTE_STATE_MACHINE_IDLE;
static uint8 counternrflashes = CONFIG_LEFT_RIGHT_NR_OF_FLASHES;
Btn_PressStateType t_LastBtnStateRight = BTN_NOT_PRESSED;
Btn_PressStateType t_LastBtnStateLeft = BTN_NOT_PRESSED;
Btn_PressStateType t_LastBtnStateHazard = BTN_NOT_PRESSED;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

void IDLE()
{
   Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 0);
   Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 0);
   Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 0);
   Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 0);
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY1, 255, DISP7SEG_BOTH_ON);
}
void RIGHT_TURN_SIGNALING()
{
   Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 0);
   Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 0);
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY1, counternrflashes, DISP7SEG_RIGHT_ON);
   if (counter < 50)
   {
      Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 10000);
      Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 10000);
   }
   else if (counter < 200)
   {
      Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 0);
      Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 0);
   }
   counter++;

   if (counter >= 200)
   {
      counter = 0;
      counternrflashes--;
   }

   if (counternrflashes < 1)
   {
      t_CurrentState = RTE_STATE_MACHINE_IDLE;
      counternrflashes = CONFIG_LEFT_RIGHT_NR_OF_FLASHES;
   }
}
void LEFT_TURN_SIGNALING()
{
   Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 0);
   Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 0);
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY1, counternrflashes, DISP7SEG_LEFT_ON);
   if (counter < 50)
   {
      Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 10000);
      Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 10000);
   }
   else if (counter < 200)
   {
      Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 0);
      Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 0);
   }
   counter++;
   if (counter >= 200)
   {
      counternrflashes--;
      counter = 0;
   }
   if (counternrflashes < 1)
   {
      t_CurrentState = RTE_STATE_MACHINE_IDLE;
      counternrflashes = CONFIG_LEFT_RIGHT_NR_OF_FLASHES;
   }
}
void HAZARD_SIGNALING()
{
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY1, 255, DISP7SEG_BOTH_OFF);
   if (counter < 50)
   {
      Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 10000);
      Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 10000);
      Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 10000);
      Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 10000);
   }
   else if (counter < 200)
   {
      Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 0);
      Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 0);
      Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 0);
      Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 0);
   }
   counter++;
   if (counter >= 200)
   {
      counter = 0;
   }
   else
   {
   }
}
static uint16 interpolare(uint16 INPUT)
{
   uint16 OUTPUT = (uint32) (INPUT - INPUT_MIN) * (OUTPUT_MAX - OUTPUT_MIN) / (INPUT_MAX - INPUT_MIN) + OUTPUT_MIN;
   return OUTPUT;
}

static uint16 interpolareConfig3(uint16 INPUT)
{
   uint16 OUTPUT3 = (uint32) (INPUT - INPUT_MIN) * (OUTPUT_MAX_CONFIG3 - OUTPUT_MIN_CONFIG3) /
      (INPUT_MAX - INPUT_MIN) + OUTPUT_MIN_CONFIG3;
   return OUTPUT3;
}
static uint16 interpolareConfig4(uint16 INPUT)
{
   uint16 OUTPUT4 = (uint32) (INPUT - INPUT_MIN) * (OUTPUT_MAX_CONFIG4 - OUTPUT_MIN_CONFIG4) /
      (INPUT_MAX - INPUT_MIN) + OUTPUT_MIN_CONFIG4;
   return OUTPUT4;
}
uint16 valInterpolare;
uint16 valoarePotentiometer;
void Config1()
{
   Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 10000);
   Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 0);
   Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 0);
   Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 0);
   valoarePotentiometer = PotMet_GetState(POTMET_INSTANCE_POT);
   valInterpolare = interpolare(valoarePotentiometer);
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY1, valInterpolare / 10, DISP7SEG_BOTH_OFF);
}
void Config2()
{
   Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 0);
   Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 0);
   Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 10000);
   Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 0);
   valoarePotentiometer = PotMet_GetState(POTMET_INSTANCE_POT);
   valInterpolare = interpolare(valoarePotentiometer);
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY1, valInterpolare / 10, DISP7SEG_BOTH_OFF);
}
void Config3()
{
   Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 0);
   Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 10000);
   Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 0);
   Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 0);
   valoarePotentiometer = PotMet_GetState(POTMET_INSTANCE_POT);
   valInterpolare = interpolareConfig3(valoarePotentiometer);
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY1, valInterpolare, DISP7SEG_BOTH_OFF);
   if (valInterpolare == 100)
   {
      Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY1, valInterpolare / 10, DISP7SEG_BOTH_ON);
   }
}
void Config4()
{
   Led_SetState(LED_INSTANCE_LED_FRONT_LEFT, 0);
   Led_SetState(LED_INSTANCE_LED_BACK_LEFT, 0);
   Led_SetState(LED_INSTANCE_LED_FRONT_RIGHT, 0);
   Led_SetState(LED_INSTANCE_LED_BACK_RIGHT, 10000);
   valoarePotentiometer = PotMet_GetState(POTMET_INSTANCE_POT);
   valInterpolare = interpolareConfig4(valoarePotentiometer);
   Disp7Seg_SetState(DISP7SEG_INSTANCE_DISPLAY1, valInterpolare, DISP7SEG_BOTH_OFF);
}
void ConfigState()
{
   Btn_PressStateType t_CurrentLeft;
   t_CurrentLeft = Btn_GetState(BTN_INSTANCE_LEFT);
   if ((t_LastBtnStateLeft == BTN_NOT_PRESSED) && (t_CurrentLeft == BTN_PRESSED))
   {
      if (t_CurrentState == RTE_STATE_MACHINE_CONFIG1)
      {
         t_CurrentState = RTE_STATE_MACHINE_CONFIG4;
      }
      else
      {
         t_CurrentState--;
      }
   }
   else
   {

   }
   Btn_PressStateType t_CurrentRight;
   t_CurrentRight = Btn_GetState(BTN_INSTANCE_RIGHT);
   if ((t_LastBtnStateRight == BTN_NOT_PRESSED) && (t_CurrentRight == BTN_PRESSED))
   {
      if (t_CurrentState == RTE_STATE_MACHINE_CONFIG4)
      {
         t_CurrentState = RTE_STATE_MACHINE_CONFIG1;
      }
      else
      {
         t_CurrentState++;
      }
   }
   else
   {

   }
   t_LastBtnStateLeft = t_CurrentLeft;
   t_LastBtnStateRight = t_CurrentRight;
   switch (t_CurrentState)
   {
      case RTE_STATE_MACHINE_IDLE:
            {

               break;
            }
            case RTE_STATE_MACHINE_LEFT_SIGNALING:
            {

               break;
            }
            case RTE_STATE_MACHINE_RIGHT_SIGNALING:
            {

               break;
            }
            case RTE_STATE_MACHINE_HAZARD_SIGNALING:
            {

               break;
            }
      case RTE_STATE_MACHINE_CONFIG1:
      {
         Config1();
         break;
      }
      case RTE_STATE_MACHINE_CONFIG2:
      {
         Config2();
         break;
      }
      case RTE_STATE_MACHINE_CONFIG3:
      {
         Config3();
         break;
      }
      case RTE_STATE_MACHINE_CONFIG4:
      {
         Config4();
         break;
      }
   }



}
void NormalState()
{
   Btn_PressStateType t_BtnCurrentHazard;
   t_BtnCurrentHazard = Btn_GetState(BTN_INSTANCE_HAZARD);
   if (t_BtnCurrentHazard == BTN_PRESSED)
   {
      if (counterForHazard <= 40U)
      {
         counterForHazard++;
      }
   }
   else
   {
      counterForHazard = 0U;
   }
   if (counterForHazard == 40U)
   {
      if (t_CurrentState == RTE_STATE_MACHINE_HAZARD_SIGNALING)
      {
         t_CurrentState = RTE_STATE_MACHINE_IDLE;
      }
      else
      {
         t_CurrentState = RTE_STATE_MACHINE_HAZARD_SIGNALING;
      }
      counter = 0U;
   }
   if (t_CurrentState == RTE_STATE_MACHINE_HAZARD_SIGNALING)
   {
      HAZARD_SIGNALING();
   }
   else
   {
   }
   t_LastBtnStateHazard = t_BtnCurrentHazard;

//RIGHT
   if (t_CurrentState != RTE_STATE_MACHINE_HAZARD_SIGNALING)
   {
      if (t_CurrentState == RTE_STATE_MACHINE_IDLE)
      {
         IDLE();
      }
      Btn_PressStateType t_CurrentRight;
      t_CurrentRight = Btn_GetState(BTN_INSTANCE_RIGHT);
      if ((t_LastBtnStateRight == BTN_PRESSED) && (t_CurrentRight == BTN_NOT_PRESSED)&&counterH > 400)
      {
         t_CurrentState = RTE_STATE_MACHINE_RIGHT_SIGNALING;
         counter = 0;
         counternrflashes = CONFIG_LEFT_RIGHT_NR_OF_FLASHES;
      }
      else
      {
      }
      if (t_CurrentState == RTE_STATE_MACHINE_RIGHT_SIGNALING)
      {
         RIGHT_TURN_SIGNALING();
      }
      else
      {
      }
      t_LastBtnStateRight = t_CurrentRight;

      //LEFT
      Btn_PressStateType t_CurrentLeft;
      t_CurrentLeft = Btn_GetState(BTN_INSTANCE_LEFT);
      if ((t_LastBtnStateLeft == BTN_PRESSED) && (t_CurrentLeft == BTN_NOT_PRESSED)&&counterH > 400 )
      {
         t_CurrentState = RTE_STATE_MACHINE_LEFT_SIGNALING;
         counter = 0;
         counternrflashes = CONFIG_LEFT_RIGHT_NR_OF_FLASHES;
      }
      else
      {
      }
      if (t_CurrentState == RTE_STATE_MACHINE_LEFT_SIGNALING)
      {
         LEFT_TURN_SIGNALING();
      }
      else
      {
      }
      t_LastBtnStateLeft = t_CurrentLeft;
   }
}

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
/**
 * \brief      Initializes RTE and all the SWCs.
 * \param      -
 * \return     -
 */
TASK(OS_INIT_TASK)
{
   IDLE();
   PotMet_Init();
   Btn_Init();
   Led_Init();
   Disp7Seg_Init();

}
/**
 * \brief      Application specific 500 us periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_FAST_TASK)
{

}

/**
 * \brief      Application specific 5 ms periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_5MS_TASK)
{
   Led_MainFunction();
   PotMet_MainFunction();
   Btn_MainFunction();
   Disp7Seg_MainFunction();

   Btn_PressStateType t_CurrentLeft;
   Btn_PressStateType t_CurrentRight;
   t_CurrentLeft = Btn_GetState(BTN_INSTANCE_LEFT);
   t_CurrentRight = Btn_GetState(BTN_INSTANCE_RIGHT);

   if ((t_CurrentLeft == BTN_PRESSED) && (t_CurrentRight == BTN_PRESSED))
   {
      if (counterH <= 400)
      {
         counterH++;
      }
   }
   else
   {
      if (counterH > 400)
      {
         if ((t_CurrentLeft == BTN_NOT_PRESSED) && (t_CurrentRight == BTN_NOT_PRESSED))
         {
            counterH = 0;
         }
      }
      else
      {
         counterH = 0;
      }
   }

   if (counterH == 400)
   {
      if (t_CurrentState <= RTE_STATE_MACHINE_HAZARD_SIGNALING)
      {
         t_CurrentState = RTE_STATE_MACHINE_CONFIG1;
      }
      else
      {
         t_CurrentState = RTE_STATE_MACHINE_IDLE;
      }
   }
   else
   {

   }

   if (t_CurrentState <= RTE_STATE_MACHINE_HAZARD_SIGNALING)
   {
      NormalState();
   }
   else
   {
     ConfigState();
   }

}


/**
 * \brief      Application specific 10 ms periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_10MS_TASK)
{

}

/**
 * \brief      Application specific 20 ms periodicity task.
 * \param      -
 * \return     -
 */
TASK(OS_20MS_TASK)
{

}

/**
 * \brief      Application specific 100 ms periodicity task.
 * \param     -
 * \return    -
 */
TASK(OS_100MS_TASK)
{
}
/**
 * \brief      Application background task.
 * \param      -
 * \return     -
 */
TASK(OS_BACKGROUND_TASK)
{
}

