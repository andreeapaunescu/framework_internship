/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Pwm_PBcfg.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Sets up the AUTOSAR PWM initialization and channel control configurations, both being included in the
 *                global exported configuration.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Pwm.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the total number of registers to be loaded in the initialization function. */
#define PWM_NUMBER_OF_INIT_REGISTERS         (11U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the configuration of all the channels.
 *
 * Each channel is defined by the addresses of the duty cycle control register and the period control register. */
static const Pwm_ChannelCtrlType Pwm_kat_ChannelsCtrlConfig[PWM_NUMBER_OF_CHANNELS] =
{
   /* PWM_CHANNEL_C_6 */
   {
      &TIM3->CCR1,            // pul_DutyCycleReg
      &TIM3->ARR,             // pul_PeriodReg
   },

   /* PWM_CHANNEL_C_7 */
   {
      &TIM3->CCR2,            // pul_DutyCycleReg
      &TIM3->ARR,             // pul_PeriodReg
   },

   /* PWM_CHANNEL_C_8 */
   {
      &TIM3->CCR3,            // pul_DutyCycleReg
      &TIM3->ARR,             // pul_PeriodReg
   },

   /* PWM_CHANNEL_C_9 */
   {
      &TIM3->CCR4,            // pul_DutyCycleReg
      &TIM3->ARR,             // pul_PeriodReg
   },
};

/** \brief  Stores the configuration of all the registers to be set in the PWM initialization function. */
static const RegInit_Masked32BitsSingleType Pwm_kat_InitLoadRegisters[PWM_NUMBER_OF_INIT_REGISTERS] =
{
   /* Select the internal clock (CK_INT) as prescaler input clock. (SMS = b000). */
   {
      &TIM3->SMCR,

      (uint32) ~(TIM_SMCR_SMS),

      0UL
   },

   /* Disable output compare 2 clear (OC2CE = 0).
    * Set output compare 2 mode to PWM mode 1 (OC2M = b110).
    * Enable output compare 2 preload. (OC2PE = 1).
    * Enable output compare 2 fast compare. (OC2FE = 1).
    * Set capture / compare 2 channel to output (CC2S = b00).
    * Disable output compare 1 clear (OC1CE = 0).
    * Set output compare 1 mode to PWM mode 1 (OC1M = b110).
    * Enable output compare 1 preload. (OC1PE = 1).
    * Enable output compare 1 fast compare. (OC1FE = 1).
    * Set capture / compare 1 channel to output (CC1S = b00). */
   {
      &TIM3->CCMR1,

      (uint32) ~(
      TIM_CCMR1_OC2CE |
      TIM_CCMR1_OC2M |
      TIM_CCMR1_OC2PE |
      TIM_CCMR1_OC2FE |
      TIM_CCMR1_CC2S |
      TIM_CCMR1_OC1CE |
      TIM_CCMR1_OC1M |
      TIM_CCMR1_OC1PE |
      TIM_CCMR1_OC1FE |
      TIM_CCMR1_CC1S),

      (uint32) (
      TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 |
      TIM_CCMR1_OC2PE |
      TIM_CCMR1_OC2FE |
      TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 |
      TIM_CCMR1_OC1PE |
      TIM_CCMR1_OC1FE)
   },

   /* Disable output compare 4 clear (OC4CE = 0).
    * Set output compare 4 mode to PWM mode 1 (OC4M = b110).
    * Enable output compare 4 preload. (OC4PE = 1).
    * Enable output compare 4 fast compare. (OC4FE = 1).
    * Set capture / compare 4 channel to output (CC4S = b00).
    * Disable output compare 3 clear (OC3CE = 0).
    * Set output compare 3 mode to PWM mode 1 (OC3M = b110).
    * Enable output compare 3 preload. (OC3PE = 1).
    * Enable output compare 3 fast compare. (OC3FE = 1).
    * Set capture / compare 3 channel to output (CC3S = b00). */
   {
      &TIM3->CCMR2,

      (uint32) ~(
      TIM_CCMR2_OC4CE |
      TIM_CCMR2_OC4M |
      TIM_CCMR2_OC4PE |
      TIM_CCMR2_OC4FE |
      TIM_CCMR2_CC4S |
      TIM_CCMR2_OC3CE |
      TIM_CCMR2_OC3M |
      TIM_CCMR2_OC3PE |
      TIM_CCMR2_OC3FE |
      TIM_CCMR2_CC3S),

      (uint32) (
      TIM_CCMR2_OC4M_2 | TIM_CCMR2_OC4M_1 |
      TIM_CCMR2_OC4PE |
      TIM_CCMR2_OC4FE |
      TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 |
      TIM_CCMR2_OC3PE |
      TIM_CCMR2_OC3FE)
   },

   /* Set capture / compare 1 to output. (CC1NP = 0).
    * Set output compare 1 polarity to active high. (CC1P = 0). Matches the configured AUTOSAR PWM channel polarity
    *    (PWM_HIGH).
    * Enable output compare 1 output pin. (CC1E = 1). */
   {
      &TIM3->CCER,

      (uint32) ~(
      TIM_CCER_CC4NP |
      TIM_CCER_CC4P |
      TIM_CCER_CC4E |
      TIM_CCER_CC3NP |
      TIM_CCER_CC3P |
      TIM_CCER_CC3E |
      TIM_CCER_CC2NP |
      TIM_CCER_CC2P |
      TIM_CCER_CC2E |
      TIM_CCER_CC1NP |
      TIM_CCER_CC1P |
      TIM_CCER_CC1E),

      (uint32) (
      TIM_CCER_CC4E |
      TIM_CCER_CC3E |
      TIM_CCER_CC2E |
      TIM_CCER_CC1E),
   },

   /* Set PWM period to 1 kHz:
    *    -ARR = f(CK_CNT) / f(PWM) = 40 MHz / 1 kHz = 40000
    * See Mcu_PBcfg.c for more information about CK_CNT.  */
   { &TIM3->ARR, (uint32) ~(0xFFFFFFFF), 40000UL },

   /* Set PWM_CHANNEL_C_6 initial PWM duty cycle to 0. */
   { &TIM3->CCR1, (uint32) ~(0xFFFFFFFF), 0UL },

   /* Set PWM_CHANNEL_C_7 initial PWM duty cycle to 0. */
   { &TIM3->CCR2, (uint32) ~(0xFFFFFFFF), 0UL },

   /* Set PWM_CHANNEL_C_8 initial PWM duty cycle to 0. */
   { &TIM3->CCR3, (uint32) ~(0xFFFFFFFF), 0UL },

   /* Set PWM_CHANNEL_C_9 initial PWM duty cycle to 0. */
   { &TIM3->CCR4, (uint32) ~(0xFFFFFFFF), 0UL },

   /* Clear count and prescale registers. */
   { &TIM3->EGR, (uint32) ~TIM_EGR_UG, TIM_EGR_UG },

   /* Enable ARR auto-reload (ARPE = 1).
    * Select edge-aligned mode (CMS = b00).
    * Set up-counting direction. (DIR = 0).
    * Set counter to not stop at update event (OPM = 0).
    * Enable update event (UDIS = 0).
    * Enable counter (CEN = 1). */
   { &TIM3->CR1, (uint32) ~(TIM_CR1_ARPE | TIM_CR1_CMS | TIM_CR1_DIR | TIM_CR1_OPM | TIM_CR1_UDIS | TIM_CR1_CEN),
      (uint32) (TIM_CR1_ARPE | TIM_CR1_CEN) },
};

/** \brief  References the configuration of all the registers to be set in the PWM initialization function and the
 *          number of registers to be initialized. */
static const RegInit_Masked32BitsConfigType Pwm_kt_InitConfig =
{ Pwm_kat_InitLoadRegisters, PWM_NUMBER_OF_INIT_REGISTERS };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Defines the post-build configuration. References the initialization configuration and the PWM channels
 *          configuration. */
const Pwm_ConfigType Pwm_gkt_Config =
{
   &Pwm_kt_InitConfig,
   Pwm_kat_ChannelsCtrlConfig,
};

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

