/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Dio_Cfg.h
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Defines and exports the AUTOSAR DIO usage of the APIs, the number of configured individual channels,
 *                channel groups and ports and the ID of each instance.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef DIO_CFG_H
#define DIO_CFG_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------- API Activation Macros ----------------------------------------------*/

/** \brief  Defines if the individual channels API is activated or not. */
#define DIO_CHANNELS_API                     (STD_ON)

/** \brief  Defines if the channel groups API is activated or not. */
#define DIO_CHANNEL_GROUPS_API               (STD_OFF)

/** \brief  Defines if the ports API is activated or not. */
#define DIO_PORTS_API                        (STD_ON)

/*---------------------------------------------- Implementation Macros ----------------------------------------------*/

/** \brief  Defines how many different port addresses are used in the configuration. */
#define DIO_NUMBER_OF_PORTS                  (3U)

/** \brief  Defines how many individual DIO channels are used in the configuration. */
#define DIO_NUMBER_OF_CHANNELS               (21U)

/** \brief  Defines how many DIO channel groups are used in the configuration. */
#define DIO_NUMBER_OF_CHANNEL_GROUPS         (0U)

/*---------------------------------------------- Individual Channel IDs ---------------------------------------------*/

#if (STD_ON == DIO_CHANNELS_API)

/* ----------------------- Port A -----------------------*/

/** \brief  Defines the ID of the port A pin 0 individual channel. */
#define DIO_CHANNEL_A_0 		               (0U)

/** \brief  Defines the ID of the port A pin 1 individual channel. */
#define DIO_CHANNEL_A_1 		               (1U)

/** \brief  Defines the ID of the port A pin 2 individual channel. */
#define DIO_CHANNEL_A_2 		               (2U)

/** \brief  Defines the ID of the port A pin 3 individual channel. */
#define DIO_CHANNEL_A_3 		               (3U)

/** \brief  Defines the ID of the port A pin 4 individual channel. */
#define DIO_CHANNEL_A_4 		               (4U)

/** \brief  Defines the ID of the port A pin 5 individual channel. */
#define DIO_CHANNEL_A_5 		               (5U)

/** \brief  Defines the ID of the port A pin 6 individual channel. */
#define DIO_CHANNEL_A_6 		               (6U)

/** \brief  Defines the ID of the port A pin 7 individual channel. */
#define DIO_CHANNEL_A_7 		               (7U)

/** \brief  Defines the ID of the port A pin 15 individual channel. */
#define DIO_CHANNEL_A_15 		               (8U)

/* ----------------------- Port B -----------------------*/

/** \brief  Defines the ID of the port B pin 0 individual channel. */
#define DIO_CHANNEL_B_0 		               (9U)

/** \brief  Defines the ID of the port B pin 1 individual channel. */
#define DIO_CHANNEL_B_1 		               (10U)

/** \brief  Defines the ID of the port B pin 2 individual channel. */
#define DIO_CHANNEL_B_2 		               (11U)

/** \brief  Defines the ID of the port B pin 3 individual channel. */
#define DIO_CHANNEL_B_3 		               (12U)

/** \brief  Defines the ID of the port B pin 4 individual channel. */
#define DIO_CHANNEL_B_4 		               (13U)

/** \brief  Defines the ID of the port B pin 5 individual channel. */
#define DIO_CHANNEL_B_5 		               (14U)

/** \brief  Defines the ID of the port B pin 6 individual channel. */
#define DIO_CHANNEL_B_6 		               (15U)

/** \brief  Defines the ID of the port B pin 7 individual channel. */
#define DIO_CHANNEL_B_7 		               (16U)

/** \brief  Defines the ID of the port B pin 15 individual channel. */
#define DIO_CHANNEL_B_15 		               (17U)

/* ----------------------- Port D -----------------------*/

/** \brief  Defines the ID of the port D pin 0 individual channel. */
#define DIO_CHANNEL_D_0                      (18U)

/** \brief  Defines the ID of the port D pin 1 individual channel. */
#define DIO_CHANNEL_D_1                      (19U)

/** \brief  Defines the ID of the port D pin 2 individual channel. */
#define DIO_CHANNEL_D_2                      (20U)

#endif

/*------------------------------------------------ Channel Group IDs ------------------------------------------------*/

#if (STD_ON == DIO_CHANNEL_GROUPS_API)
/** \brief  Defines the ID of the port D pins 14 to 15 channel group. */
#define DIO_CHANNEL_GROUP_D_14_15            (&Dio_gkat_ChannelGroups[0U])

/** \brief  Defines the ID of the port D pins 13 to 14 channel group. */
#define DIO_CHANNEL_GROUP_D_13_14            (&Dio_gkat_ChannelGroups[1U])

/** \brief  Defines the ID of the port A pins 1 to 2 channel group. */
#define DIO_CHANNEL_GROUP_A_1_2              (&Dio_gkat_ChannelGroups[2U])
#endif

/*----------------------------------------------------- Port IDs ----------------------------------------------------*/

/** \brief  Defines the ID of the port A. */
#define DIO_PORT_A                           (0U)

/** \brief  Defines the ID of the port B. */
#define DIO_PORT_B                           (1U)

/** \brief  Defines the ID of the port D. */
#define DIO_PORT_D                           (2U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* DIO_CFG_H */
